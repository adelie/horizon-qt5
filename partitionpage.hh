#ifndef PARTITIONPAGE_HH
#define PARTITIONPAGE_HH

#include "horizonwizardpage.hh"

#ifndef NOT_NATIVE
#    include <QPushButton>
#endif

class PartitionPage : public HorizonWizardPage
{
public:
        PartitionPage(QWidget *parent = nullptr);

        int nextId() const;
private:
#ifndef NOT_NATIVE
        QPushButton *openEditor;
#endif
};

#endif // SOFTWAREPAGE_HH
