#ifndef SOFTWAREPAGE_HH
#define SOFTWAREPAGE_HH

#include "horizonwizardpage.hh"

#include <QButtonGroup>
#include <QCheckBox>

class SoftwarePage : public HorizonWizardPage
{
public:
        SoftwarePage(QWidget *parent = 0);

        int nextId() const;
        void toggleDesktops(bool show);
private:
        QButtonGroup *typeGroup;
        QCheckBox *desktop, *server, *advanced,
                  *kde, *xfce, *lxqt, *openbox;
};

#endif // SOFTWAREPAGE_HH
