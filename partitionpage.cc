#include "partitionpage.hh"
#include "horizonwizard.hh"

#ifndef NOT_NATIVE
#       include <QHBoxLayout>
#       include <QProcess>
#endif

#include <QLabel>
#include <QVBoxLayout>

PartitionPage::PartitionPage(QWidget *parent) : HorizonWizardPage (parent)
{
        QLabel *infoLabel;
        QVBoxLayout *layout = new QVBoxLayout;
#ifndef NOT_NATIVE
        QHBoxLayout *buttonLayout = new QHBoxLayout;
#endif

        loadWatermark("partition");
        setTitle(tr("Partition Disk Drive"));

        infoLabel = new QLabel(tr(
#ifdef NOT_NATIVE
                    "You must partition the disk before continuing.  Unfortunately the Horizon Wizard cannot help you at this time."
#else
                    "You must partition your computer's disk drive(s) before "
                    "you can install the Adélie Linux system.\n\nIf you have "
                    "already partitioned your computer's disk drive(s) before "
                    "beginning this installation, choose Next.  Otherwise, "
                    "choose \"Launch Partitioner\" to start the disk "
                    "partitioning process.\n\n"

                    "NOTE: Incorrectly partitioning your computer's disk "
                    "drive(s) may cause data loss, or require further "
                    "partitioning in order to boot.  Consult the owner's "
                    "manual for your computer and/or firmware for more "
                    "information."
#endif
        ));
        infoLabel->setWordWrap(true);

        layout->addWidget(infoLabel);
#ifndef NOT_NATIVE
        openEditor = new QPushButton(tr("Launch Partitioner"));
        QObject::connect(openEditor, static_cast<void (QPushButton:: *)(bool)>(&QPushButton::clicked),
                         [](bool checked) {
            QProcess *parted = new QProcess;

        });
        buttonLayout->addWidget(openEditor, 0, Qt::AlignCenter);
        layout->addLayout(buttonLayout);
#endif
        this->setLayout(layout);
}

int PartitionPage::nextId() const
{
#ifdef NOT_NATIVE
        return HorizonWizard::Page_Networking;
#else
        if(this->horizonWizard()->interfaces.size() > 0)
        {
                return HorizonWizard::Page_Networking;
        } else {
                return HorizonWizard::Page_Software;
        }
#endif
}
